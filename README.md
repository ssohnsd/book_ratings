# Project Book Ratings

Overview

In this project, a book review website is created with Python Flask and Heroku/PostgreSQL. Users are able to register for the website and then log in using their username and password. Once they log in, they will be able to search for books, leave reviews for individual books, and see the reviews made by other people. A third-party API by Goodreads, another book review website, is used to pull in ratings from a broader audience. Finally, users will be able to query for book details and book reviews programmatically via the website’s API.

Steps:

Navigate to https://www.heroku.com/, and create an account.
On Heroku’s Dashboard, click “New” and choose “Create new app.”
Give app a name, and click “Create app.”
On app’s “Overview” page, click the “Configure Add-ons” button.
In the “Add-ons” section of the page, type in and select “Heroku Postgres.”
Choose the “Hobby Dev - Free” plan, which will give you access to a free PostgreSQL database that will support up to 10,000 rows of data. Click “Provision.”
Now, click the “Heroku Postgres :: Database” link.

On the database’s overview page, Click on “Settings”, and then “View Credentials.” This is the information to log into the database. Access the database via Adminer, filling in the server (the “Host” in the credentials list), username (the “User”), password, and the name of the database, all of which can find on the Heroku credentials page.

Run pip3 install -r requirements.txt in terminal window to make sure that all of the necessary Python packages (Flask and SQLAlchemy, for instance) are installed.

Set the environment variable FLASK_APP to be application.py using export FLASK_APP=application.py. Optionally set the environment variable FLASK_DEBUG to 1, which will activate Flask’s debugger and will automatically reload  web application whenever save a change to a file.

Set the environment variable DATABASE_URL to be the URI of database.

Run flask run to start up Flask application.

Go to https://www.goodreads.com/api and sign up for a Goodreads account.
Navigate to https://www.goodreads.com/api/keys and apply for an API key. For “Application name” and “Company name” feel free to just write “project1,” and no need to incluce an application URL, callback URL, or support URL.
Should then see API key. (For this project, we’ll care only about the “key”, not the “secret”.)
Now use that API key to make requests to the Goodreads API, documented here. In particular, Python code like the below

import requests
res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"key": "KEY", "isbns": "9781632168146"})
print(res.json())

where KEY is your API key, will give the review and rating data for the book with the provided ISBN number. In particular, might see something like this dictionary:

{'books': [{
                'id': 29207858,
                'isbn': '1632168146',
                'isbn13': '9781632168146',
                'ratings_count': 0,
                'reviews_count': 1,
                'text_reviews_count': 0,
                'work_ratings_count': 26,
                'work_reviews_count': 113,
                'work_text_reviews_count': 10,
                'average_rating': '4.04'
            }]
}

Note that work_ratings_count here is the number of ratings that this particular book has received, and average_rating is the book’s average score out of 5.

Requirements

Registration: Users should be able to register for your website, providing (at minimum) a username and password.

Login: Users, once registered, should be able to log in to your website with their username and password.

Logout: Logged in users should be able to log out of the site.

Import: Provided in this project is a file called books.csv, which is a spreadsheet in CSV format of 5000 different books. Each one has an ISBN number, a title, an author, and a publication year. In a Python file called import.py separate from web application, write a program that will take the books and import them into the PostgreSQL database. First need to decide what table(s) to create, what columns those tables should have, and how they should relate to one another. Run this program by running python3 import.py to import the books into database.

Search: Once a user has logged in, they should be taken to a page where they can search for a book. Users should be able to type in the ISBN number of a book, the title of a book, or the author of a book. After performing the search, website should display a list of possible matching results, or some sort of message if there were no matches. If the user typed in only part of a title, ISBN, or author name, search page should find matches for those as well!

Book Page: When users click on a book from the results of the search page, they should be taken to a book page, with details about the book: its title, author, publication year, ISBN number, and any reviews that users have left for the book on the website.

Review Submission: On the book page, users should be able to submit a review: consisting of a rating on a scale of 1 to 5, as well as a text component to the review where the user can write their opinion about a book. Users should not be able to submit multiple reviews for the same book.

Goodreads Review Data: On the book page, should also display (if available) the average rating and number of ratings the work has received from Goodreads.

API Access: If users make a GET request to your website’s /api/<isbn> route, where <isbn> is an ISBN number, website should return a JSON response containing the book’s title, author, publication date, ISBN number, review count, and average score. The resulting JSON should follow the format:
{
    "title": "Memory",
    "author": "Doug Lloyd",
    "year": 2015,
    "isbn": "1632168146",
    "review_count": 28,
    "average_score": 5.0
}
If the requested ISBN number isn’t in your database, should return a 404 error.

Using raw SQL commands (as via SQLAlchemy’s execute method) in order to make database queries.

At minimum, one table to keep track of users, one table to keep track of books, and one table to keep track of reviews. 

In terms of how to “log a user in,” recall that can store information inside of the session, which can store different values for different users. In particular, if each user has an id, then could store that id in the session (e.g., in session["user_id"]) to keep track of which user is currently logged in.


