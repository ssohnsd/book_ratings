import os
import requests

from flask import Flask, session, render_template, request, jsonify
from flask_session import Session
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from bcrypt import hashpw, gensalt, checkpw

app = Flask(__name__)
app.secret_key = os.getenv("APP_SECRET_KEY")

if not os.getenv("DATABASE_URL"):
    raise RuntimeError("DATABASE_URL is not set")

GOODREADS_KEY = os.getenv("GOODREADS_KEY")
if not GOODREADS_KEY:
    raise RuntimeError("GOODREADS_KEY is not set")

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))

@app.route("/")
def index():
    message = ""
    return render_template("index.html", message=message)

@app.route("/register")
def register():
    message = ""
    return render_template("register.html", message=message)

@app.route('/logout')
def logout():
    session.pop('username', None)
    message = "You are logged out."
    return render_template("index.html", message=message)

@app.route("/books", methods=["GET", "POST"])
def books():
    prevpage = request.form.get("previous")
    if prevpage == "login":
        username = request.form.get("username")
        password = request.form.get("password")
        user = db.execute("SELECT * FROM users WHERE username = :username", {"username": username}).fetchone()
        if user is None:
            return render_template("index.html", message="Username does not exit. Please register.")
        user = db.execute("SELECT * FROM users WHERE username = :username", \
            {"username": username}).fetchone()
        password = password.encode('utf-8')
        if not checkpw(password, bytes.fromhex(user.password[2:])):
            return render_template("index.html", message="Password does not match username. \
                Please re-enter password.")
        session['username'] = username
    elif prevpage == "register":
        username = request.form.get("username")
        password = request.form.get("password")
        encoded = password.encode('utf8')
        hashed = hashpw(encoded, gensalt())
        user = db.execute("SELECT * FROM users WHERE username = :username", {"username": username}).fetchone()
        if user is not None:
            return render_template("register.html", message="Username already exits. Please provide a different one.")
        if username != "" and password != "":
            db.execute("INSERT INTO users (username, password) VALUES (:username, :password)", \
                {"username": username, "password": hashed})
            db.commit()
            session['username'] = username
        else:
            return render_template("register.html", message="Username or password not entered.")
                    
    qtitle = request.form.get("title")
    qauthor = request.form.get("author")
    qisbn = request.form.get("isbn")

    if qtitle:
        qtitle = '%' + qtitle.upper() + '%'
    if qauthor:
        qauthor = '%' + qauthor.upper() + '%'
    if qisbn:
	    qisbn = '%' + qisbn.upper() + '%'

    books = db.execute("SELECT * FROM books WHERE upper(title) LIKE :qtitle \
                OR upper(author) LIKE :qauthor OR upper(isbn) LIKE :qisbn ", \
                {"qtitle":qtitle, "qauthor":qauthor, "qisbn":qisbn}).fetchall()

    return render_template("books.html", books=books)

@app.route("/books/<string:isbn>",  methods=["GET", "POST"])
def book(isbn):
    """Lists details about a single book."""

    book = db.execute("SELECT * FROM books WHERE isbn = :isbn", {"isbn": isbn}).fetchone()
    if book is None:
        return render_template("error.html", message="No such book.")

    goodreads_num_ratings = 0
    goodreads_ave_rating = 0
    try:
        res = requests.get("https://www.goodreads.com/book/review_counts.json", \
            params={"key": GOODREADS_KEY, "isbns": isbn})
        res = res.json()['books'][0]
        goodreads_num_ratings = res['work_ratings_count']
        goodreads_ave_rating = res['average_rating']
    except:
        print("Error in Goodreads request")

    book_id = book.book_id

    #Try to get user's review
    message = ""
    content = request.form.get("review")
    try:
        rating = int(request.form.get("rating"))
    except:
        rating = ""

    if content == None and rating == "":
        stmt = ("SELECT users.username AS user,\
            reviews.rating AS rating, reviews.content AS content \
            FROM reviews LEFT OUTER JOIN users ON users.id = reviews.id \
            WHERE book_id = {}").format(book_id)

        reviews = db.execute(stmt).fetchall()
        return render_template("book.html", book=book, reviews=reviews, \
            goodreads_num_ratings=goodreads_num_ratings, goodreads_ave_rating=goodreads_ave_rating, message=message)

    username = session['username']
    user = db.execute("SELECT * FROM users WHERE username = :username", {"username": username}).fetchone()
    userReview = db.execute("SELECT * FROM reviews WHERE book_id = :book_id AND id = :id", \
        {"book_id": book_id, "id":user.id}).fetchone()
    if userReview is None:
        if content != "" and (rating == 1 or rating == 2 or rating == 3 or rating == 4 or rating == 5) :
            db.execute("INSERT INTO reviews (rating, content, book_id, id) \
                VALUES (:rating, :content, :book_id, :id)", \
                {"rating":rating, "content":content, "book_id":book_id, "id":user.id})
            db.commit()
        else:
            message = "Error in review/rating. \
                Please enter text for review and rating of 1,2,3,4 or 5 where 1 is poor and 5 is excellent."
    else:
        if content != "" and (rating == 1 or rating == 2 or rating == 3 or rating == 4 or rating == 5) :
            db.execute("UPDATE reviews SET rating = :rating, content = :content \
                WHERE book_id = :book_id AND id = :id", \
                {"rating":rating, "content":content, "book_id":book_id, "id":user.id})
            db.commit()
        else:
            message = "Error in review/rating. \
                Please enter text for review and rating of 1,2,3,4 or 5 where 1 is poor and 5 is excellent."

    # Get all reviews
    stmt = ("SELECT users.username AS user,\
            reviews.rating AS rating, reviews.content AS content \
            FROM reviews LEFT OUTER JOIN users ON users.id = reviews.id \
            WHERE book_id = {}").format(book_id)
    reviews = db.execute(stmt).fetchall()
    return render_template("book.html", book=book, reviews=reviews, \
        goodreads_num_ratings=goodreads_num_ratings, goodreads_ave_rating=goodreads_ave_rating, message=message)


@app.route("/api/<string:isbn>")
def book_api(isbn):
    book = db.execute("SELECT * FROM books WHERE isbn = :isbn", {"isbn": isbn}).fetchone()
    if book is None:    
        return jsonify({"error": "Invalid ISBN"}), 404

    goodreads_num_ratings = 0
    goodreads_ave_rating = 0
    try:
        res = requests.get("https://www.goodreads.com/book/review_counts.json", \
            params={"key": GOODREADS_KEY, "isbns": isbn})
        res = res.json()['books'][0]
        goodreads_num_ratings = res['work_ratings_count']
        goodreads_ave_rating = res['average_rating']
    except:
        print("Error in Goodreads request")

    book_id = book.book_id

    return jsonify({
            "title": book.title,
            "author": book.author,
            "year": book.year,
            "isbn": book.isbn,
            "review_count": goodreads_num_ratings,
            "average_score": goodreads_ave_rating
        })
